import Koa from 'koa';
import Apollo from 'apollo-server-koa';
import resolvers from './entities/resolvers.js';
import typeDefs from './entities/typeDefs.js';
import Headless from './headless.js';

const { ApolloServer } = Apollo;

const app = new Koa();
const headless = new Headless();
const apollo = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ ctx }) => ({
        getClientConfig: async (ctx) => headless.getClientConfig(ctx),
        ctx
    })
});

app.use(apollo.getMiddleware({ path: process.env.APOLLO_API_PATH }));

app.listen({ port: process.env.KOA_PORT }, () => {
    console.log('Server is up and ready!');
    console.log(`Server Address: http://localhost:${process.env.KOA_PORT}`);
    console.log(`GraphQL Address: http://localhost:${process.env.KOA_PORT}${process.env.APOLLO_API_PATH}`);
});

