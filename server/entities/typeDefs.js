import Apollo from 'apollo-server-koa';

const { gql } = Apollo;

const typeDefs = [gql`
    type Query {
        productSearch(query: String!): SearchResult
    }

    type SearchResult {
        limit: Int!
        hits: [ProductHit]
    }


    type ProductHit {
        productId: String!
        productName: String!
    }
`];

export default typeDefs;
