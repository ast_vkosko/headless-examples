import CommerceSDK from 'commerce-sdk';

const search = async (query, ctx) => {
    let clientConfig = await ctx.getClientConfig(ctx);
    const searchClient = new CommerceSDK.Search.ShopperSearch(clientConfig);

    return searchClient.productSearch({ parameters: { q: query } });
};

const resolvers = {
    Query: {
        productSearch: async (_, { query }, ctx) => {
            let result;

            try {
                result = await search(query, ctx);
            } catch (error) {
                console.log(error);
            }

            return result;
        }
    }
};

export default resolvers;
