import CommerceSDK from 'commerce-sdk';

export default class Headless {
    constructor(config) {
        this.config = this.getConfig();
        CommerceSDK.sdkLogger.setLevel(CommerceSDK.sdkLogger.levels.INFO);
    }

    getConfig () {
        return {
            headers: {},
            parameters: {
                clientId: process.env.COMMERCE_CLIENT_CLIENT_ID,
                organizationId: process.env.COMMERCE_CLIENT_ORGANIZATION_ID,
                shortCode: process.env.COMMERCE_CLIENT_SHORT_CODE,
                siteId: process.env.COMMERCE_CLIENT_API_SITE_ID
            }
        };
    }

    async getClientConfig (ctx) {
        let clientConfig = this.config;
        let token;

        try {
            let response = await CommerceSDK.helpers.getShopperToken(this.config, { type: 'guest' });

            token = response.getBearerHeader();
        } catch (error) {
            console.error(error);
            return null;
        }

        clientConfig.headers.authorization = token;

        return clientConfig;
    }
}
